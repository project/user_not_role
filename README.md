# User Not Role

Provides a "User Not Role" condition that acts in the opposite behavior of the
Drupal core "User Role" condition. A site builder may add this to their block
visibility settings to only show a block for users that do not have a role.

## Configuration

1. Enable the module on the Administration > Extend page.
2. Go to the Administration > Structure > Block layout page.
3. Press the Configure button for a block.
4. Click the "User Not Role" vertical tab.
5. Select roles that a user must not have to display the block.
6. Press the "Save block" button.

Common scenario: A user has two roles: "shouldshow" and "shouldhide". There is a
block that should only be visible to "shouldshow", but not "shouldhide".

Solution: Configure the block with the "User Role" visibility setting to
"shouldshow" and the "User Not Role" visibility setting to "shouldhide".

## Technical documentation

This takes the intersection of the configured roles and a user account's roles.
If there is no intersection between the two, then the user must not have the
role and the condition passes.

When negated, the opposite occurs. If there is no intersection, the condition
fails.

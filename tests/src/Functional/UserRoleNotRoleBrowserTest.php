<?php

namespace Drupal\Tests\user_not_role\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\block\Traits\BlockCreationTrait;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Tests mix of user_role and user_not_role conditions.
 *
 * @group user_not_role
 */
class UserRoleNotRoleBrowserTest extends BrowserTestBase {

  use BlockCreationTrait;
  use UserCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'node',
    'user',
    'block',
    'system',
    'user_not_role',
  ];

  /**
   * A user account with the "shouldshow" and "shouldhide" role.
   *
   * @var \Drupal\User\UserInterface
   */
  protected $hideUser;

  /**
   * A user account without the "shouldshow" role.
   *
   * @var \Drupal\User\UserInterface
   */
  protected $showUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->createAdminRole('admin', 'Administrator');

    $role = $this->drupalCreateRole(['access content'], 'shouldshow', 'Should show');
    $secondRole = $this->drupalCreateRole(['access content'], 'shouldhide', 'Should hide');

    $settings = [
      'label_display' => TRUE,
      'visibility' => [
        'user_role' => [
          'roles' => ['shouldshow' => 'shouldshow'],
          'context_mapping' => [
            'user' => '@user.current_user_context:current_user',
          ],
        ],
        'user_not_role' => [
          'roles' => ['shouldhide' => 'shouldhide'],
          'context_mapping' => [
            'user' => '@user.current_user_context:current_user',
          ],
          'negate' => FALSE,
        ],
      ],
    ];

    $block = $this->drupalPlaceBlock('system_powered_by_block', $settings);
    $block->enable();
    $block->save();

    $this->showUser = $this->createUser([]);
    $this->showUser->addRole('shouldshow');
    $this->showUser->save();

    $this->hideUser = $this->createUser([]);
    $this->hideUser->addRole($role);
    $this->hideUser->addRole($secondRole);
    $this->hideUser->save();
  }

  /**
   * Tests visibility of block when both user_role and user_not_role in use.
   */
  public function testVisibility() {
    $this->drupalLogin($this->showUser);
    $this->assertSession()->pageTextContains('Powered by Drupal');

    $this->drupalLogin($this->hideUser);
    $this->assertSession()->pageTextNotContains('Powered by Drupal');
  }

}

<?php

namespace Drupal\Tests\user_not_role\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\block\Traits\BlockCreationTrait;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Provides functional tests for user_not_role condition using block visibility.
 *
 * @group user_not_role
 */
class UserNotRoleBrowserTest extends BrowserTestBase {

  use BlockCreationTrait;
  use UserCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'node',
    'user',
    'block',
    'system',
    'user_not_role',
  ];

  /**
   * A user account with the "privileged" role.
   *
   * @var \Drupal\User\UserInterface
   */
  protected $privilegedUser;

  /**
   * A user account without the "privileged" role.
   *
   * @var \Drupal\User\UserInterface
   */
  protected $unprivilegedUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->createAdminRole('admin', 'Administrator');

    $role = $this->drupalCreateRole(['access content'], 'privileged', 'Privileged');
    $secondRole = $this->drupalCreateRole(['access content'], 'second', 'Second role');
    $this->unprivilegedUser = $this->createUser([]);
    $this->privilegedUser = $this->createUser([]);
    $this->privilegedUser->addRole($role);
    $this->privilegedUser->addRole($secondRole);
    $this->privilegedUser->save();
  }

  /**
   * Tests the condition when added to a block.
   *
   * @param array $expectations
   *   An associative array keyed by class property and expected visibility.
   * @param array $provided_settings
   *   Block settings and block visibility settings.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Drupal\Core\Entity\EntityStorageException
   *
   * @dataProvider blockSettingsProvider
   */
  public function testVisibility(array $expectations, array $provided_settings) {
    $block = $this->drupalPlaceBlock('system_powered_by_block', $provided_settings);
    $block->enable();
    $block->save();

    $this->drupalGet(Url::fromRoute('<front>'));
    if ($expectations['anonymous']) {
      $this->assertSession()->pageTextContains('Powered by Drupal');
    }
    else {
      $this->assertSession()->pageTextNotContains('Powered by Drupal');
    }

    $this->drupalLogin($this->privilegedUser);
    if ($expectations['privilegedUser']) {
      $this->assertSession()->pageTextContains('Powered by Drupal');
    }
    else {
      $this->assertSession()->pageTextNotContains('Powered by Drupal');
    }

    $this->drupalLogin($this->unprivilegedUser);
    if ($expectations['unprivilegedUser']) {
      $this->assertSession()->pageTextContains('Powered by Drupal');
    }
    else {
      $this->assertSession()->pageTextNotContains('Powered by Drupal');
    }
  }

  /**
   * Provides test cases for block visibility settings.
   *
   * @return array[]
   *   Test cases.
   */
  public static function blockSettingsProvider() {
    return [
      'visible to all' => [
        [
          'privilegedUser' => TRUE,
          'unprivilegedUser' => TRUE,
          'anonymous' => TRUE,
        ],
        [
          'label_display' => TRUE,
          'visibility' => [
            'user_not_role' => [
              'roles' => [],
              'negate' => FALSE,
            ],
          ],
        ],
      ],
      'visible to unprivileged, anonymous' => [
        [
          'privilegedUser' => FALSE,
          'unprivilegedUser' => TRUE,
          'anonymous' => TRUE,
        ],
        [
          'label_display' => TRUE,
          'visibility' => [
            'user_not_role' => [
              'roles' => ['privileged' => 'privileged'],
              'context_mapping' => [
                'user' => '@user.current_user_context:current_user',
              ],
              'negate' => FALSE,
            ],
          ],
        ],
      ],
      'not visible to privileged when 1 role configured' => [
        [
          'privilegedUser' => FALSE,
          'unprivilegedUser' => TRUE,
          'anonymous' => TRUE,
        ],
        [
          'label_display' => TRUE,
          'visibility' => [
            'user_not_role' => [
              'roles' => ['privileged' => 'privileged'],
              'context_mapping' => [
                'user' => '@user.current_user_context:current_user',
              ],
              'negate' => FALSE,
            ],
          ],
        ],
      ],
      'visible to unprivileged, anonymous with 2 roles' => [
        [
          'privilegedUser' => FALSE,
          'unprivilegedUser' => TRUE,
          'anonymous' => TRUE,
        ],
        [
          'label_display' => TRUE,
          'visibility' => [
            'user_not_role' => [
              'roles' => ['privileged' => 'privileged', 'second' => 'second'],
              'context_mapping' => [
                'user' => '@user.current_user_context:current_user',
              ],
              'negate' => FALSE,
            ],
          ],
        ],
      ],
      'visible to privileged when negated' => [
        [
          'privilegedUser' => TRUE,
          'unprivilegedUser' => FALSE,
          'anonymous' => FALSE,
        ],
        [
          'label_display' => TRUE,
          'visibility' => [
            'user_not_role' => [
              'roles' => ['privileged' => 'privileged'],
              'context_mapping' => [
                'user' => '@user.current_user_context:current_user',
              ],
              'negate' => TRUE,
            ],
          ],
        ],
      ],
      'visible to privileged+second when negated' => [
        [
          'privilegedUser' => TRUE,
          'unprivilegedUser' => FALSE,
          'anonymous' => FALSE,
        ],
        [
          'label_display' => TRUE,
          'visibility' => [
            'user_not_role' => [
              'roles' => ['privileged' => 'privileged', 'second' => 'second'],
              'context_mapping' => [
                'user' => '@user.current_user_context:current_user',
              ],
              'negate' => TRUE,
            ],
          ],
        ],
      ],
    ];
  }

}

<?php

namespace Drupal\Tests\user_not_role\Unit\Plugin\Condition;

use Drupal\Component\Plugin\Definition\ContextAwarePluginDefinitionInterface;
use Drupal\Component\Plugin\Definition\ContextAwarePluginDefinitionTrait;
use Drupal\Component\Plugin\Definition\PluginDefinition;
use Drupal\Core\Config\Entity\ConfigEntityType;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Plugin\Context\ContextDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Tests\UnitTestCase;
use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;
use Drupal\user_not_role\Plugin\Condition\UserNotRole;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * Provides unit test cases for user_not_role condition plugin.
 *
 * @group user_not_role
 */
class UserNotRoleTest extends UnitTestCase {

  use ProphecyTrait;

  /**
   * Tests the condition summary method.
   *
   * @param string $expectedText
   *   The expected translatable string.
   * @param string[]|null $expectedParams
   *   The expected translation parameters.
   * @param array $configuration
   *   The plugin configuration.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\Exception\EntityTypeIdLengthException
   *
   * @dataProvider summaryProvider
   */
  public function testSummary(string $expectedText, ?array $expectedParams, array $configuration): void {
    $storageProphet = $this->prophesize('\Drupal\Core\Config\Entity\ConfigEntityStorageInterface');
    $storageProphet->loadMultiple()->willReturn([
      new Role([
        'id' => RoleInterface::ANONYMOUS_ID,
        'label' => 'Anonymous',
      ], 'user_role'),
      new Role([
        'id' => RoleInterface::AUTHENTICATED_ID,
        'label' => 'Authenticated',
      ], 'user_role'),
      new Role([
        'id' => 'test_role',
        'label' => 'Test role',
      ], 'user_role'),
      new Role([
        'id' => 'test_role_2',
        'label' => 'Test role 2',
      ], 'user_role'),
    ]);
    $managerProphet = $this->prophesize('\Drupal\Core\Entity\EntityTypeManagerInterface');
    $managerProphet->getStorage('user_role')->willReturn($storageProphet->reveal());
    $managerProphet
      ->getDefinition('user_role')
      ->willReturn(new ConfigEntityType([
        'id' => 'user_role',
        'entity_keys' => [
          'id' => 'id',
          'weight' => 'weight',
          'label' => 'label',
        ],
      ]));

    $manager = $managerProphet->reveal();
    $container = new ContainerBuilder();
    $container->set('entity_type.manager', $manager);
    \Drupal::setContainer($container);

    $translationProphet = $this->prophesize('\Drupal\Core\StringTranslation\TranslationInterface');
    $string_translation = $translationProphet->reveal();

    // phpcs:ignore Drupal.Semantics.FunctionT.NotLiteralString
    $expectedMarkup = new TranslatableMarkup($expectedText, $expectedParams, [], $string_translation);

    $definition = new ContextAwarePluginDefinition();
    $condition = new UserNotRole(
      $configuration,
      'user_not_role',
      $definition,
      $manager
    );
    $condition->setStringTranslation($string_translation);

    $this->assertEquals($expectedMarkup, $condition->summary());
  }

  /**
   * Tests the condition evaluation.
   *
   * @param bool $expected
   *   The expected value.
   * @param string[] $roles
   *   A list of role ids to assign to the account interface.
   * @param array $configuration
   *   The plugin configuration to use for the condition plugin instance.
   *
   * @dataProvider evaluateProvider
   */
  public function testEvaluate(bool $expected, array $roles = [], array $configuration = []): void {
    $account = $this->prophesize('\Drupal\Core\Session\AccountInterface');
    $account->getRoles()->willReturn($roles);

    $dataProphet = $this->prophesize('\Drupal\Core\TypedData\ComplexDataInterface');
    $dataProphet->getValue()->willReturn($account->reveal());

    // Prophecy does not support object chaining because it is poorly
    // opinionated. And what's more, we cannot mock the interface because the
    // methods are only on the class because.
    $mockEntityDefinition = $this
      ->createMock('\Drupal\Core\TypedData\DataDefinition');

    $mockEntityDefinition->method('setLabel')->willReturn($mockEntityDefinition);
    $mockEntityDefinition->method('setDescription')->willReturn($mockEntityDefinition);
    $mockEntityDefinition->method('setRequired')->willReturn($mockEntityDefinition);
    $mockEntityDefinition->method('getConstraints')->willReturn([]);

    $typedDataProphet = $this->prophesize('\Drupal\Core\TypedData\TypedDataManagerInterface');
    $typedDataProphet->createDataDefinition('entity:user')->willReturn($mockEntityDefinition);
    $typedDataProphet->create($mockEntityDefinition, Argument::any())->willReturn($dataProphet->reveal());

    $storageProphet = $this->prophesize('\Drupal\Core\Config\Entity\ConfigEntityStorageInterface');
    $storageProphet->loadMultiple()->willReturn([
      new Role([
        'id' => RoleInterface::ANONYMOUS_ID,
        'label' => 'Anonymous',
      ], 'user_role'),
    ]);
    $managerProphet = $this->prophesize('\Drupal\Core\Entity\EntityTypeManagerInterface');
    $managerProphet->getStorage('user_role')->willReturn($storageProphet->reveal());
    $manager = $managerProphet->reveal();

    $container = new ContainerBuilder();
    $container->set('typed_data_manager', $typedDataProphet->reveal());
    $container->set('entity_type.manager', $manager);
    \Drupal::setContainer($container);

    $contextProphet = $this->prophesize('\Drupal\Core\Plugin\Context\Context');
    $contextProphet->getContextValue()->willReturn($account->reveal());

    // This has to be "any" because Drupal core asserts that it has to be
    // EntityContextDefinition, which is much more convoluted and code smelly so
    // whatever.
    $context_definition = new ContextDefinition('any');
    $definition = new ContextAwarePluginDefinition();
    $definition->addContextDefinition('user', $context_definition);
    $condition = new UserNotRole(
      $configuration,
      'user_not_role',
      $definition,
      $manager
    );

    $condition->setContext('user', $contextProphet->reveal());

    if ($condition->isNegated()) {
      $this->assertEquals(!$expected, $condition->evaluate());
    }
    else {
      $this->assertEquals($expected, $condition->evaluate());
    }
  }

  /**
   * Data provider for tests.
   *
   * @return array
   *   An array of test cases.
   */
  public static function evaluateProvider() {
    return [
      'True when no roles selected' => [
        TRUE,
        [RoleInterface::ANONYMOUS_ID],
        ['roles' => [], 'negate' => FALSE],
      ],
      'True when user does not have role' => [
        TRUE,
        [RoleInterface::ANONYMOUS_ID],
        [
          'roles' => [
            RoleInterface::AUTHENTICATED_ID => RoleInterface::AUTHENTICATED_ID,
          ],
          'negate' => FALSE,
        ],
      ],
      'False when user has role' => [
        FALSE,
        [RoleInterface::AUTHENTICATED_ID],
        [
          'roles' => [
            RoleInterface::AUTHENTICATED_ID => RoleInterface::AUTHENTICATED_ID,
          ],
          'negate' => FALSE,
        ],
      ],
      'False when user does not have role negated' => [
        FALSE,
        [RoleInterface::ANONYMOUS_ID],
        [
          'roles' => [
            RoleInterface::AUTHENTICATED_ID => RoleInterface::AUTHENTICATED_ID,
          ],
          'negate' => TRUE,
        ],
      ],
      'True when user has role negated' => [
        TRUE,
        [RoleInterface::AUTHENTICATED_ID],
        [
          'roles' => [
            RoleInterface::AUTHENTICATED_ID => RoleInterface::AUTHENTICATED_ID,
          ],
          'negate' => TRUE,
        ],
      ],
      'False when user only has one role' => [
        FALSE,
        [RoleInterface::AUTHENTICATED_ID],
        [
          'roles' => [
            RoleInterface::AUTHENTICATED_ID => RoleInterface::AUTHENTICATED_ID,
            'test_role' => 'test_role',
          ],
          'negate' => FALSE,
        ],
      ],
      'False when user has all roles' => [
        FALSE,
        [RoleInterface::AUTHENTICATED_ID, 'test_role'],
        [
          'roles' => [
            RoleInterface::AUTHENTICATED_ID => RoleInterface::AUTHENTICATED_ID,
            'test_role' => 'test_role',
          ],
          'negate' => FALSE,
        ],
      ],
      'True when user has all roles, negated' => [
        TRUE,
        [RoleInterface::AUTHENTICATED_ID, 'test_role'],
        [
          'roles' => [
            RoleInterface::AUTHENTICATED_ID => RoleInterface::AUTHENTICATED_ID,
            'test_role' => 'test_role',
          ],
          'negate' => TRUE,
        ],
      ],
      'True when user only has one role, negated' => [
        TRUE,
        [RoleInterface::AUTHENTICATED_ID],
        [
          'roles' => [
            RoleInterface::AUTHENTICATED_ID => RoleInterface::AUTHENTICATED_ID,
            'test_role' => 'test_role',
          ],
          'negate' => TRUE,
        ],
      ],
      'True when user only has more than one role, negated' => [
        TRUE,
        [RoleInterface::AUTHENTICATED_ID, 'test_role'],
        [
          'roles' => [
            RoleInterface::AUTHENTICATED_ID => RoleInterface::AUTHENTICATED_ID,
            'test_role' => 'test_role',
            'test_role_2' => 'test_role_2',
          ],
          'negate' => TRUE,
        ],
      ],
      'True when user only has all roles, negated' => [
        TRUE,
        [RoleInterface::AUTHENTICATED_ID, 'test_role', 'test_role_2'],
        [
          'roles' => [
            RoleInterface::AUTHENTICATED_ID => RoleInterface::AUTHENTICATED_ID,
            'test_role' => 'test_role',
            'test_role_2' => 'test_role_2',
          ],
          'negate' => TRUE,
        ],
      ],
    ];
  }

  /**
   * Provides test cases for testing summary method.
   *
   * @return array
   *   An array of test cases.
   */
  public static function summaryProvider(): array {
    return [
      [
        'No restriction',
        [],
        [
          'roles' => [],
          'negate' => FALSE,
        ],
      ],
      [
        'The user is not any of @roles',
        ['@roles' => 'Authenticated'],
        [
          'roles' => [
            RoleInterface::AUTHENTICATED_ID => RoleInterface::AUTHENTICATED_ID,
          ],
          'negate' => FALSE,
        ],
      ],
      [
        'The user is not any of @roles',
        ['@roles' => 'Test role, Test role 2'],
        [
          'roles' => [
            'test_role' => 'test_role',
            'test_role_2' => 'test_role_2',
          ],
          'negate' => FALSE,
        ],
      ],
      [
        'The user can be one of @roles',
        ['@roles' => 'Authenticated'],
        [
          'roles' => [
            RoleInterface::AUTHENTICATED_ID => RoleInterface::AUTHENTICATED_ID,
          ],
          'negate' => TRUE,
        ],
      ],
      [
        'The user can be one of @roles',
        ['@roles' => 'Test role, Test role 2'],
        [
          'roles' => [
            'test_role' => 'test_role',
            'test_role_2' => 'test_role_2',
          ],
          'negate' => TRUE,
        ],
      ],
    ];
  }

}

/**
 * Defines a context-aware plugin definition.
 */
class ContextAwarePluginDefinition extends PluginDefinition implements ContextAwarePluginDefinitionInterface {
  use ContextAwarePluginDefinitionTrait;

}

<?php

namespace Drupal\user_not_role\Plugin\Condition;

use Drupal\Component\Plugin\Exception\ContextException;
use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a negated condition for User Role.
 *
 * @Condition(
 *   id = "user_not_role",
 *   label = @Translation("User Not Role"),
 *   context_definitions = {
 *     "user" = @ContextDefinition("entity:user", label = @Translation("User"))
 *   }
 *  )
 */
final class UserNotRole extends ConditionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Initialization method.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin identifier.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity_type.manager service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('When the user does not have the following roles'),
      '#default_value' => $this->configuration['roles'],
      '#options' => array_map('\Drupal\Component\Utility\Html::escape', $this->getUserRoles()),
      '#description' => $this->t('If you select no roles, the condition will evaluate to TRUE for all users regardless of the All of behavior.'),
    ];
    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'roles' => [],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['roles'] = array_filter($form_state->getValue('roles'));
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {
    // Use the role labels. They will be sanitized below.
    $roles = array_intersect_key($this->getUserRoles(), $this->configuration['roles']);
    if (empty($this->configuration['roles'])) {
      return $this->t('No restriction');
    }
    elseif (count($roles) > 1) {
      $roles = implode(', ', $roles);
    }
    else {
      $roles = reset($roles);
    }

    if (!empty($this->configuration['negate'])) {
      return $this->t('The user can be one of @roles', [
        '@roles' => $roles,
      ]);
    }

    return $this->t('The user is not any of @roles', [
      '@roles' => $roles,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    if (empty($this->configuration['roles']) && !$this->isNegated()) {
      return TRUE;
    }

    try {
      /** @var \Drupal\Core\Session\AccountInterface $user */
      $user = $this->getContextValue('user');
    }
    catch (ContextException $e) {
      return FALSE;
    }

    // If there is even 1 role in the intersection then the user has that role
    // so we evaluate to FALSE.
    return empty(array_intersect($this->configuration['roles'], $user->getRoles()));
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    // Optimizes cache context, if a user cache context is provided, only use
    // user.roles, since that's the only part this condition cares about.
    $contexts = [];
    foreach (parent::getCacheContexts() as $context) {
      $contexts[] = $context === 'user' ? 'user.roles' : $context;
    }
    return $contexts;
  }

  /**
   * Gets the user role names.
   *
   * @return string[]
   *   The user role ids.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getUserRoles(): array {
    $role_map = [];
    $roles = $this
      ->entityTypeManager
      ->getStorage('user_role')
      ->loadMultiple();
    foreach ($roles as $role) {
      $id = $role->id();
      $role_map[$id] = $role->label();
    }

    return $role_map;
  }

}
